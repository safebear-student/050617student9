from page_objects import PageObject,PageElement

class MainPage(PageObject):
    logout_link = PageElement (link_text="Logout")
    def check_page(self):
        return "Login" in self.w.title


    def logout (self) :
         self.logout_link.click()
