import base_test

class TestCases (base_test.BaseTest) :
    def test_01_test_login_page(self):
        # Step 1: Click on Login and Login page loads
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login to the webpage and confirm the main page appears
        assert self.loginpage.login(self.mainpage, "testuser", "testing")
        # Step 3: Logout of the website and confirm the login page is returned
        assert self.mainpage.click_login(self.mainpage)
